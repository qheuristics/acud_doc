acud
====

acud is a lightweight capacity expansion, economic dispatch and unit commitment simulation
tool written in Python which uses csv and Excel files as input and output data format.
It is an optimisation-based simulation tool that leverages multiple state-of-art data handling
Python packages including pandas and xarray, powered by open source (e.g. glpk, cbc) or 
commercial (e.g. cplex, gurobi) solver engines. The optimisation is defined as a 
Linear Programming (LP) or Mixed-Integer Linear Programming (MIP) problem, depending on the 
desired level of accuracy and complexity. It is able to handle multiple time resolutions,
producing both chronological and load duration curve-based results.

Features
--------

- Capacity expansion
- Minimum and maximum power for each unit
- Generation unit derating
- Power plant ramping limits
- Minimum up/down times
- Curtailment
- Detailed hydro modelling, including hydro cascades
- Pumped storage
- Non-dispatchable units (e.g. wind, solar, run-of-river, etc.)
- Start-up, ramping and no-load costs
- Net transfer capacity based network modelling
- Constraints on the targets for renewables and/or CO2 emissions

Output
------
- Timing, location and type of new capacity built
- Nodal energy prices
- Scheduled generation
- Generating unit starts and stops
- Fuel consumption
- Emissions
- Interconnector flows
- Storage volumes, water releases and spillages
- Pumped energy
- Unserved energy

Installation
------------

We strongly suggest that you create a dedicated conda environment for acud.

License
-------

The project is licensed under the MIT license.
