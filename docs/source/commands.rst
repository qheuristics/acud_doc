=================
Command reference
=================

.. contents::
   :local:
   :depth: 1

The ``ad`` command is the primary interface for managing 
acud simulations.

.. raw:: html

        <div>
        <PRE>
        <B>ACUD(1)</B>                         User Commands                        <B>ACUD(1)</B>


        Usage: ad [OPTIONS] COMMAND [ARGS]...



          acud: capacity expansion, unit commitment and

          dispatch simulation engine



        Options:

          --version  Display version.

          --help     Show this message and exit.



        Commands:

          dry       Dry run model.

          run       Run model simulation.

          schema    Display input data schemas.

          validate  Validate input data.

          xtra      Extrapolate solution.

  
        </PRE></div>

acud provides several commands for data handling and execution of power market simulations.
The links on this page provide help for each command.
You can also access help from the command line with the
``--help`` flag:

.. code-block:: bash

   ad run --help

TIP: You can abbreviate many frequently used command options that 
are preceded by 2 dashes (``--``) to just 1 dash and the first 
letter of the option. So ``--list`` and ``-n`` are the same.   
   
Execution commands
==================

Execution of acud simulations is managed through the following commands:

.. toctree::
   :glob:
   :maxdepth: 2

   commands/run/*

Data commands
=============

acud input data handling commands include:

.. toctree::
   :glob:
   :maxdepth: 2
   
   commands/data/*