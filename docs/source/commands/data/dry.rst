------
ad dry
------

.. raw:: html

        <div>
        <PRE>
        <B>ACUD(1)</B>                         User Commands                        <B>ACUD(1)</B>


        Usage: ad dry [OPTIONS] RUN_CONFIG



          Dry run the given RUN_CONFIG run configuration

          file.



        Options:

          --default  Copy built-in default file into

                     current directory.

          --run      Copy run template file into current

                     directory.

          --model    Copy model template file into current

                     directory.

          --help     Show this message and exit.

                           
        </PRE></div>
