---------
ad schema
---------

.. raw:: html

        <div>
        <PRE>
        <B>ACUD(1)</B>                         User Commands                        <B>ACUD(1)</B>


        Usage: ad schema [OPTIONS] SCHEMAS



          Display input data schema.



          SCHEMAS is the name of the schema to be

          displayed, e.g.



              ad schema generator



          To list the available schemas, pass the -l

          flag followed by any string as SCHEMAS (it

          does not need to be a valid schema name) like

          this:



              ad schema -l acme



        Options:

          -l      Display list of available schemas.

          --help  Show this message and exit.

              
        </PRE></div>
