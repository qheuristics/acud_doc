-----------
ad validate
-----------

.. raw:: html

        <div>
        <PRE>
        <B>ACUD(1)</B>                         User Commands                        <B>ACUD(1)</B>


        Usage: ad validate [OPTIONS] RUN



          Validate the given run and model configuration

          files.



        Options:

          -a, --all                Run complete

                                   validation, including

                                   csv files.

          --verbose                Display verbose output.

          --debug                  Print debug information

                                   when encountering

                                   errors.

          --pdb                    If used together with

                                   --debug, drop into

                                   interactive debugger on

                                   encountering errors.

          --profile                Run through cProfile.

          --profile_filename TEXT  Filename to save

                                   profile to if enabled

                                   --profile.

          --help                   Show this message and

                                   exit.

                           
        </PRE></div>
