-------
ad xtra
-------

.. raw:: html

        <div>
        <PRE>
        <B>ACUD(1)</B>                         User Commands                        <B>ACUD(1)</B>


        Usage: ad xtra [OPTIONS] RUN_CONFIG



          Post-process solution to extrapolate results

          as required.



        Options:

          --debug                  Print debug information

                                   when encountering

                                   errors.

          --pdb                    If used together with

                                   --debug, drop into

                                   interactive debugger on

                                   encountering errors.

          --profile                Run through cProfile.

          --profile_filename TEXT  Filename to save

                                   profile to if enabled

                                   --profile.

          --help                   Show this message and

                                   exit.

                           
        </PRE></div>
