============
Unit testing
============

The acud developer distribution includes basic
`pytest-based <https://docs.pytest.org/en/latest/>`_ unit testing.
To run these unit tests you first need to install pytest either
via conda:

.. code-block :: console

    conda install pytest

or pip:

.. code-block :: console

    pip install pytest
    
Once pytest is installed, set the test folder as current directory:

.. code-block :: console
    
    cd test

To run all available pytest unit test:

.. code-block :: console

    py.test -v

or

.. code-block :: console

    python -m pytest -v

To run a specific unit test:

.. code-block :: console

    py.test test_api.py -v

or 

.. code-block :: console

    python -m pytest test_cli.py -v

Note that passing the validate test included in the unit test suite means that
the validation process completed successfully, not that the validation tests
passed. To review the validation test results, run the validate command 
separately through the command line or via the acud Python api.
