====
acud
====

acud is a lightweight capacity expansion, economic dispatch and unit commitment 
simulation tool written in Python, which uses csv and Excel files as input and output data format.
It is an optimisation-based calculation engine that leverages multiple state-of-art data handling
Python packages including pandas and xarray, powered by open source (e.g. glpk, cbc) or 
commercial (e.g. cplex, gurobi) solvers. 

.. toctree::
   :hidden:
   :maxdepth: 1

   user-guide/index
   commands
   developer-guide/index
   license
