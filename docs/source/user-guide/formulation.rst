===========
Formulation
===========

A partial formulation of the chronological MT stage mathematical optimization problem can be found below.

.. raw:: html
    
    <iframe src="https://drive.google.com/file/d/1dbG2pPM-s31pnmnn80eEfPXw0KKZpCgn/preview" width="640" height="480"></iframe>