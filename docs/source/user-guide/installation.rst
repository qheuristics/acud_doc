============
Installation
============

We strongly recommend using a dedicated conda environment to install acud.

Install the package with pip::

    $ pip install git+[acud git repository url here]
