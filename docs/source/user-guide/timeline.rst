===================
Simulation timeline
===================

A typical acud simulation timeline is a sequence of contiguous hours
between the start and the end simulation datetimes, both included.
For sizeable models with an extended timeline, this can lead to very long
runtimes (hours) and massive hardrive memory consumption.
A workaround for this is to run acud simulations with a snapshots-based timeline.

Snapshots-based simulation timeline
-----------------------------------

When running a simulation through a snapshots-based timeline, , you can specify
a set of (non-contiguous) datetimes at which the optimization should be run. If required,
these snapshots-based simulation results can then be extrapolated, assigning the nearest hourly results
to non-computed datetimes (e.g. the extrapolated result of a non-computed datetime happening at 7a.m.
will be the last computed result at 7 a.m.).

Snapshots are specified by:

1. Listing snapshots in a csv file with a single field called datetime, and
#. Referencing this csv file in a ``time.snapshots.file`` entry in the run configuration yml file.

Note that you also need to provide the date format in the time.snapshots.datetimeformat entry to ensure
that the snapshot dates and times are correctly interpreted by acud. An extract of the time section of
a run yml file including snapshots-based timeline specification is shown below.

.. code-block:: console

    time:
        startperiod: 2015-01-01 00:00
        endperiod: 2018-12-31 23:00
        stepsize: 24
        blocks: 3
        snapshots:
            file: csv/snapshots.csv
            datetimeformat: '%d/%m/%Y'

To further illustrate, these are the first lines of an example snapshots csv file.
Note that the dates are non-contiguous.            

.. code-block:: console

    datetime
    01/01/2018
    03/01/2018
    10/01/2018
    20/01/2018
            
Snapshots override start and end datetime specifications. If no snapshots specification is provided
in the run config yml file, the simulation timeline will consist of the sequence of contiguous hours
between the start and the end datetime, both included.

After completing a snapshots-based simulation, you can *extrapolate* the sparse timeline results
to obtain a dense timeline solution using the ``xtra`` command, e.g. like this:

.. code-block:: console

    ad xtra run.yml
